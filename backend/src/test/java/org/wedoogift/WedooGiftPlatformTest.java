package org.wedoogift;

import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WedooGiftPlatformTest {


    @Test(expected = InvalidCompanyException.class)
    public void can_not_create_gift_card_with_invalid_company(){
        List<Company> companies = new ArrayList<>();
        companies.add(new Company(1,"wedoogift",100));
        List<User> users = new ArrayList<>();
        users.add(new User(1));
        WedooGiftPlatform platform = new WedooGiftPlatform();
        platform.setCompanies(companies);
        platform.setUsers(users);

        platform.sendGiftCard(2,1,50,LocalDate.now());
    }

    @Test(expected = InvalidUserException.class)
    public void can_not_send_gift_card_to_invalid_user(){
        List<Company> companies = new ArrayList<>();
        companies.add(new Company(1,"wedoogift",100));
        List<User> users = new ArrayList<>();
        users.add(new User(1));
        WedooGiftPlatform platform = new WedooGiftPlatform();
        platform.setCompanies(companies);
        platform.setUsers(users);

        platform.sendGiftCard(1,2,50,LocalDate.now());
    }

    @Test(expected = InsufficientFundException.class)
    public void can_not_send_gift_card_when_company_balance_is_insufficient(){
        List<Company> companies = new ArrayList<>();
        companies.add(new Company(1,"wedoogift",100));
        List<User> users = new ArrayList<>();
        users.add(new User(1));
        WedooGiftPlatform platform = new WedooGiftPlatform();
        platform.setCompanies(companies);
        platform.setUsers(users);

        platform.sendGiftCard(1,1,150,LocalDate.now());
    }

    @Test
    public void new_distribution_is_added_when_gift_card_is_sent(){
        List<Company> companies = new ArrayList<>();
        companies.add(new Company(1,"wedoogift",100));
        List<User> users = new ArrayList<>();
        users.add(new User(1));
        WedooGiftPlatform platform = new WedooGiftPlatform();
        platform.setCompanies(companies);
        platform.setUsers(users);
        platform.setDistributions(new ArrayList<>());

        platform.sendGiftCard(1,1,50, LocalDate.now());
        assertEquals(1,platform.getDistributions().size());
    }

}