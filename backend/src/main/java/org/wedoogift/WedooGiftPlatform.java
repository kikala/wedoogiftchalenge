package org.wedoogift;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.List;

public class WedooGiftPlatform {

    private List<Wallet> wallets;

    private List<Company> companies;

    private List<User> users;

    private List<Endowment> distributions;

    public WedooGiftPlatform() {
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Endowment> getDistributions() {
        return distributions;
    }

    public void setDistributions(List<Endowment> distributions) {
        this.distributions = distributions;
    }

    public void sendGiftCard(int companyId, int userId, long amount, LocalDate startDate){
        Company company = this.findCompanyById(companyId);
        User user = this.findUserById(userId);
        GiftCard endowment = company.distributeGiftCard(user,amount,startDate);
        this.distributions.add(endowment);
    }

    public void sendMealVoucher(int companyId, int userId, long amount, LocalDate startDate){
        Company company = this.findCompanyById(companyId);
        User user = this.findUserById(userId);
        MealVoucher mealVoucher = company.distributeMealVoucher(user,amount,startDate);
        this.distributions.add(mealVoucher);
    }

    private Company findCompanyById(int companyId){
        return this.companies
                .stream()
                .filter(c -> c.getId() == companyId)
                .findFirst()
                .orElseThrow(() -> new InvalidCompanyException("Company with id = " + companyId +" does not exist"));
    }

    private User findUserById(int userId){
        return this.users
                .stream()
                .filter(u -> u.getId() == userId)
                .findFirst()
                .orElseThrow(() -> new InvalidUserException("User with id = " + userId +" does not exist"));
    }
}
