package org.wedoogift;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

public class App {

    public static void main(String...args){
        String inputPath = "backend/Level2/data/input.json";
        String outputPath = "backend/Level2/data/output.json";
        Path input = Paths.get(inputPath).toAbsolutePath().normalize();

        try (InputStream in = Files.newInputStream(input)){
            ObjectMapper mapper = new ObjectMapper();
            WedooGiftPlatform platform = mapper.readValue(in,WedooGiftPlatform.class);
            platform.sendGiftCard(1,1,50, LocalDate.of(2020,9,16));
            platform.sendGiftCard(1,2,100, LocalDate.of(2020,8,1));
            platform.sendGiftCard(2,3,1000, LocalDate.of(2020,5,1));
            //j'ai modifié la date de debut a 2021 pour que la carte reste valide
            platform.sendMealVoucher(1,1,250,LocalDate.of(2021,5,1));
            Path output = Paths.get(outputPath).toAbsolutePath().normalize();
            Files.writeString(output,mapper.writeValueAsString(platform));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InsufficientFundException|InvalidCompanyException|InvalidUserException e){
            System.out.println(e.getMessage());
        }
    }
}
