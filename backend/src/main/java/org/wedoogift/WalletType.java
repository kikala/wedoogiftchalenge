package org.wedoogift;

public enum WalletType {

    GIFT(1),
    FOOD(2);

    private int id;

    private WalletType(int id){this.id = id;}

    public int getId() {
        return id;
    }
}
