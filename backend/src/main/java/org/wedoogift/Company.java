package org.wedoogift;

import java.time.LocalDate;
import java.util.Objects;

public class Company {

    private int id;

    private String name;

    private long balance;

    public Company() {
    }

    public Company(int id, String name, long balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public GiftCard distributeGiftCard(User user, long amount,LocalDate startDate){
        if(this.balance - amount < 0 ){
            throw new InsufficientFundException("There is no enough money for this distribution");
        }
        this.balance -= amount;
        return new GiftCard(this,user,amount, startDate);
    }

    public MealVoucher distributeMealVoucher(User user, long amount,LocalDate startDate){
        if(this.balance - amount < 0 ){
            throw new InsufficientFundException("There is no enough money for this distribution");
        }
        this.balance -= amount;
        return new MealVoucher(this,user,amount, startDate);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getBalance() {
        return balance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return id == company.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
