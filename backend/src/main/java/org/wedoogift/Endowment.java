package org.wedoogift;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Clock;
import java.time.LocalDate;
import java.util.Objects;

public abstract class Endowment {

    private static int NB_ENDOWMENT = 0;

    protected int id;

    private long amount;

    private Company company;

    private User user;

    protected LocalDate startDate;

    //clock for fixing date during tests
    protected Clock clock;

    public Endowment(Company company, User user,long amount,LocalDate startDate) {
        NB_ENDOWMENT++;
        this.id = NB_ENDOWMENT;
        this.amount = amount;
        this.company = company;
        this.user = user;
        this.startDate = startDate;
    }

    @JsonIgnore
    public boolean isNotExpired(){
        LocalDate now;
        if(this.clock == null){
            now = LocalDate.now();
        } else {
            now = LocalDate.now(this.clock);
        }
        return now.isBefore(this.getExpiryDate());
    }

    @JsonIgnore
    protected abstract LocalDate getExpiryDate();

    @JsonProperty("wallet_id")
    public abstract int getWalletId();

    public long getAmount() {
        return amount;
    }

    public int getId() {
        return id;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    @JsonIgnore
    public Company getCompany() {
        return company;
    }

    @JsonIgnore
    public LocalDate getStartDate() {
        return startDate;
    }

    @JsonProperty("start_date")
    public String getStartDateAsString(){
       return startDate.toString();
    }

    @JsonProperty("end_date")
    public String getEndDateAsString(){
      return this.getExpiryDate().minusDays(1).toString();
    }

    @JsonProperty("company_id")
    public int getCompanyId(){
        return this.company.getId();
    }

    @JsonProperty("user_id")
    public int getUserId(){
        return this.user.getId();
    }

    @JsonIgnore
    public void setClock(Clock clock) {
        this.clock = clock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Endowment endowment = (Endowment) o;
        return id == endowment.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
