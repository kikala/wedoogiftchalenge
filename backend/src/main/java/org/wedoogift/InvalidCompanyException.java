package org.wedoogift;

public class InvalidCompanyException extends RuntimeException{

    public InvalidCompanyException(String message) {
        super(message);
    }
}
