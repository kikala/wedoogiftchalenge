package org.wedoogift.web.auth;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class FakeApplicationUserDao implements ApplicationUserDao{

    private final PasswordEncoder passwordEncoder;

    public FakeApplicationUserDao(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ApplicationUser> selectApplicationUserByUserName(String userName) {
        return this.getApplicationUsers()
                .stream()
                .filter(u -> u.getUsername().equals(userName))
                .findFirst();
    }

    private List<ApplicationUser> getApplicationUsers(){
        List<ApplicationUser> applicationUsers = List.of(
                new ApplicationUser("wedoogift",passwordEncoder.encode("password"),
                        getGrantedAuthorities(),true),
                new ApplicationUser("unknown",passwordEncoder.encode("password"),
                        new HashSet<>(),true)
        );
        return applicationUsers;
    }

    private Set<SimpleGrantedAuthority> getGrantedAuthorities(){
        return Set.of(new SimpleGrantedAuthority("wedooGiftUser"));
    }

}
