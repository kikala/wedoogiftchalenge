 package org.wedoogift;

public class Wallet {

    private int id;

    private String name;

    private WalletType type;


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(WalletType type) {
        this.type = type;
    }

    public Wallet() {
    }
}
