package org.wedoogift;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {

    private int id;

    private UserWallet giftCardWallet;

    private UserWallet mealVoucherWallet;

    public User(){}

    public User(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void addGiftCard(GiftCard giftCard){
        if(this.giftCardWallet == null){
            this.giftCardWallet = new UserWallet(WalletType.GIFT.getId(),0);
        }
        this.giftCardWallet.addEndowment(giftCard);
    }

    public void addMealVoucher(MealVoucher mealVoucher){
        if(this.mealVoucherWallet == null){
            this.mealVoucherWallet = new UserWallet(WalletType.FOOD.getId(),0);
        }
        this.mealVoucherWallet.addEndowment(mealVoucher);
    }

    @JsonProperty("balance")
    public void setWallets(List<UserWallet> wallets) {
        this.giftCardWallet = wallets.stream().filter(w -> w.getId() == WalletType.GIFT.getId())
                .findFirst().orElse(null);
        this.mealVoucherWallet = wallets.stream().filter(w -> w.getId() == WalletType.FOOD.getId())
                .findFirst().orElse(null);
    }

    @JsonProperty("balance")
    public List<UserWallet> getWallets() {
        List<UserWallet> wallets = new ArrayList<>();
        if(this.giftCardWallet != null){
            wallets.add(this.giftCardWallet);
        }
        if(this.mealVoucherWallet != null){
            wallets.add(this.mealVoucherWallet);
        }
        return wallets;
    }

    @JsonIgnore
    public UserWallet getGiftCardWallet() {
        return giftCardWallet;
    }

    @JsonIgnore
    public UserWallet getMealVoucherWallet() {
        return mealVoucherWallet;
    }

    @JsonIgnore
    public void setGiftCardWallet(UserWallet giftCardWallet) {
        this.giftCardWallet = giftCardWallet;
    }

    @JsonIgnore
    public void setMealVoucherWallet(UserWallet mealVoucherWallet) {
        this.mealVoucherWallet = mealVoucherWallet;
    }

    public boolean hasGiftCard(GiftCard giftCard){
        if(this.giftCardWallet == null){
            return false;
        }
        return this.giftCardWallet.hasEndowment(giftCard);
    }

    public boolean hasMealVoucher(MealVoucher mealVoucher){
        if(this.mealVoucherWallet == null){
            return false;
        }
        return this.mealVoucherWallet.hasEndowment(mealVoucher);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
