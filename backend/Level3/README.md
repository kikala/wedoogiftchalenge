# Level 3

In this level we are expecting:
* A REST controller for the functions implemented in level 1 and 2.
* Secure the endpoints using spring security. feel free to choose the security mechanism. 

# Implementation
* The endpoint is localhost:8080/send, run the WebApp class to start it
* The endpoint is secured using spring security JWT.
* To call this endpoint we must have the 'wedooGiftUser' role
* Two users exists (wedoogift/password) and (unknown/password) , only user with username = wedoogift has wedooGiftUser role
#Test
* Connect to the application using localhost:8080/login with body {"username": "user","password":"password"} and get the token returned
* call the endpoint /send with the token in the AUTHORIZATION header preceded by 'Bearer '
